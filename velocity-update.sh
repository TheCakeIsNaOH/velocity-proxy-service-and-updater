#!/bin/bash
set -euo pipefail

cd /path/to/velocity/directory
UPDATE_PAGE=$(curl -s https://www.velocitypowered.com/downloads)
UPDATE_URL=$(echo $UPDATE_PAGE | grep -oi 'https://ci.velocitypowered.com/.*jar')
UPDATE_VERSION=$(echo $UPDATE_URL | grep -io 'proxy-.*jar' | grep -ioP '\d.*\d')
CURRENT_VERSION=$(cat version.txt)

if [ $CURRENT_VERSION == $UPDATE_VERSION ]
then
        echo Velocity up to date: version $CURRENT_VERSION
else
        echo New Velocity update available: version $UPDATE_VERSION
        systemctl stop velocity.service
        mv velocity-proxy.jar velocity-proxy.jar.old
        curl -o velocity-proxy.jar $UPDATE_URL
        systemctl start velocity.service
        echo $UPDATE_VERSION > version.txt
        rm velocity-proxy.jar.old
fi