# Velocity Minecraft Proxy Systemd service and updater

## SystemD service file

In `velocity.service`, edit `WorkingDirectory` and `ExecStart` to point the correct paths for your velocity install and your java binary respectively. Put it in `/etc/systemd/system`, although that may vary depending on distro. 

Run 
```
sudo systemctl daemon-reload
sudo systemctl enable velocity
sudo systemctl start velocity
```

to get up and running.


## Updater script

Edit `velocity-updater.sh` as needed to `cd` to your velocity install path.

Echo your current version into `version.txt` in the same directory as your velocity jar. Ex `echo 1.0.7 > version.txt`

Needs the systemd service setup and running.

This scrapes the page more or less, so it is fragile and likely to be broken.